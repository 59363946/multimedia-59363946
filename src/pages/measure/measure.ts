import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the MeasurePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-measure',
  templateUrl: 'measure.html',
})
export class MeasurePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  slider = [
    {
      title:'1.วัดพระศรีรัตนมหาธาตุ',
      description:'วัดพระศรีรัตนมหาธาตุ หรือ วัดพระบรมธาตุเมืองเชลียง และเรียกอีกชื่อว่า วัดพระปรางค์ เป็นกลุ่มโบราณสถานขนาดใหญ่ มีโบราณสถานที่สำคัญภายในวัด ได้แก่ ปรางค์ประธาน ก่อด้วยศิลาแลงฉาบปูน บริเวณเรือนธาตุด้านหน้ามีบันไดขึ้นองค์ปรางค์สู่ซุ้มโถง และทางด้านขวามีพระพุทธรูปปูนปั่นปางลีลาที่มีลักษณะงดงาม กำแพงวัดเป็นศิลาและแท่นกลมขนาดใหญ่เรียงชิดติดกันเป็นรูปสี่เหลี่ยมผืนผ้า กว้าง 60  เมตร ยาว 90  เมตร ',
      image:"assets/imgs/photo2.jpg"
    },
    {
      title:'2.วัดเขาพนมเพลิง',
      description:'ตั้งอยู่บนยอดเขาพนมเพลิงภายในกำแพงเมือง มีโบราณสถานที่สำคัญคือ เจดีย์ประธานทรงกลม และมณฑปก่อด้วยศิลาแลง ฐานสี่เหลี่ยมจัตุรัสยกพื้นสูง หลังคาโค้งแหลม มีบันไดทางขึ้นสู่มณฑป ชาวบ้านเรียกว่า ศาลเจ้าแม่ละอองสำลี  ส่วนทางขึ้นวัดสามารถขึ้นได้ 2 เส้นทาง คือ จากด้านหน้าวัดแก่งหลวง และด้านข้างวัดซึ่งทางขึ้นได้ทำเป็นบันไดศิลาแลง ระหว่างทางขึ้นทั้งสองด้านมีศาลาที่พักด้วย  ',
      image:"assets/imgs/photo3.jpg"
    },
    {
      title:'3.วัดเขาสุวรรณคีรี ',
      description:'อยู่ทางทิศตะวันตก ถัดจากเขาพนมเพลิงไปประมาณ 200 เมตร โดยตั้งอยู่บนเขาอีกยอดหนึ่งในเทือกเขาเดียวกัน กลุ่มโบราณสถานที่สำคัญ คือ เจดีย์ประธานทรงกลมองค์ระฆังขนาดใหญ่ก่อด้วยศิลาแลง ฐานเขียงใหญ่    5 ชั้น ใช้สำหรับเป็นลานประทักษิณ มีซุ้มพระทั้ง 4 ด้าน ตรงก้านฉัตรมีพระพุทธรูปปูนปั้นปางลีลาเดินจงกรมรอบก้านฉัตรเช่นเดียวกับวัดช้างล้อม ',
      image:"assets/imgs/photo4.jpg"
    },
    {
      title:'4.วัดช้างล้อม',
      description:'อยู่ภายในกำแพงเมืองศรีสัชนาลัย โบราณสถานที่สำคัญคือ เจดีย์ประธานทรงลังกา ตั้งอยู่ภายในกำแพงแก้วสี่เหลี่ยมจัตุรัส ที่ฐานเจดีย์มีช้างปูนปั้นยืนหันหลังชนผนังเจดีย์อยู่โดยรอบ จำนวน 39 เชือก และช้างที่อยู่ตามมุมเจดีย์ทั้ง 4 ทิศ ตกแต่งเป็นช้างทรงเครื่อง มีลวดลายปูนปั้นประดับที่คอ ต้นขา และข้อเท้า ',
      image:"assets/imgs/photo5.jpg"
    },
    {
      title:'5.วัดเจดีย์เจ็ดแถว ',
      description:'ตั้งอยู่ด้านหน้าวัดช้างล้อม มีเจดีย์แบบต่างๆ มากมายที่เป็นศิลปะสุโขทัยแท้ และเป็นศิลปะแบบศรีวิชัยผสมสุโขทัย โบราณสถานที่สำคัญคือ เจดีย์ประธานรูปดอกบัวตูมอยู่ด้านหลังพระวิหาร และมีเจดีย์รายรวมทั้งอาคารขนาดเล็กแบบต่าง ๆ จำนวน 33 องค์ มีกำแพงแก้วล้อมรอบอีกชั้นหนึ่ง ',
      image:"assets/imgs/photo6.jpg"
    },
    {
      title:'6.วัดสวนแก้วอุทยานใหญ่ ',
      description:'ตั้งอยู่ภายในกำแพงเมือง ซึ่งอยู่ไม่ไกลจากวัดเจดีย์เจ็ดแถวนัก โบราณสถานที่สำคัญคือ เจดีย์ประธานทรงกลม ก่อด้วยศิลาแลง องค์ระฆังได้พังทลายลง ด้านหน้ามีบันไดขึ้นไปจากมุขหลังของวิหารไปถึงเรือนธาตุเพื่อสักการะพระพุทธรูป ด้านเจดีย์ประธานมีวิหาร มีมุขด้านหน้า และด้านหลัง มีบันไดขึ้น 5 ทาง เสาวิหาร และกำแพงวัดก่อด้วยศิลาแลง  ',
      image:"assets/imgs/photo7.jpg"
    },
    {
      title:'7.วัดสวนแก้วอุทยานน้อย ',
      description:'หรือเรียกกันอีกชื่อว่า วัดสระแก้ว อยู่ห่างจากวัดช้างล้อม 200 เมตร กลุ่มโบราณสถานมีกำแพงแก้วล้อมรอบ มีประตูทางเข้าด้านหน้า และด้านหลังวัด มีโบราณสถานประกอบด้วย เจดีย์ประธานเป็นเจดีย์ทรงดอกบัวตูมล้อมรอบด้วยกำแพงแก้ว วิหารมีซุ้มพระตั้งอยู่ด้านหลังลักษณะเป็นมณฑป หลังคามณฑปเป็นรูปโค้งแหลม ภายในประดิษฐานพระพุทธรูปประทับนั่งปางมารวิชัย  ',
      image:"assets/imgs/photo8.jpg"
    },
    {
      title:'8.วัดนางพญา  ',
      description:'ตั้งอยู่แนวเดียวกันกับวัดสวนแก้วอุทยานใหญ่ เป็นวัดที่มีลวดลายปูนปั้นงดงาม ปรากฏอยู่บนซากผนังวิหารด้านตะวันตกเฉียงเหนือ ภายในวิหารตามเสาทุกด้านมีเทพนม เจดีย์ประธานของวัดเป็นเจดีย์ทรงกลมตั้งอยู่บนฐานประทักษิณ ซุ้มด้านหน้ามีบันไดทางขึ้นจนถึงภายในโถงเจดีย์ ตรงกลางโถงมีแกนเจดีย์ประดับด้วยลวดลายปูนปั้น วิหารก่อด้วยศิลาแลงมีมุขหน้า และมุขหลัง ',
      image:"assets/imgs/photo9.jpg"
    },
    {
      title:'9.วัดชมชื่น  ',
      description:'ห่างจากวัดพระศรีรัตนมหาธาตุมาทางทิศตะวันออกประมาณ 400 เมตร โบราณสถานที่สำคัญคือ เจดีย์ประธานทางกลมก่อด้วยศิลาแลง วิหารอยู่ด้านหน้าเจดีย์ประธานก่อด้วยศิลาแลงขนาด 6 ห้อง มีมุขยื่นออกมาด้านหน้า ด้านหลังพระวิหารเชื่อมต่อกับมณฑป ด้านหน้าทั้งสองข้างมณฑปทำเป็นซุ้มจรนัม 2 ซุ้ม ด้านหลังมีซุ้มจรนัมที่ประดิษฐานพระพุทธรูปนาคปรก แต่ปัจจุบันได้สูญหายไปแล้ว ',
      image:"assets/imgs/photo10.jpg"
    },
  ];

  

}
