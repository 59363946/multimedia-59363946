import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { HistoryPage } from '../history/history';
import { MeasurePage } from '../measure/measure';
import { PricePage } from '../price/price';
import { TravelPage} from '../travel/travel';


/**
 * Generated class for the Part1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-part1',
  templateUrl: 'part1.html',
})
export class Part1Page {


  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  goTohistoryPage(){
    this.navCtrl.push(HistoryPage);
  } 
  goTomeasurePage(){
    this.navCtrl.push(MeasurePage);
  }  
  goTopricePage (){
    this.navCtrl.push(PricePage);
  } 
  goTotravelPage(){
    this.navCtrl.push(TravelPage);
  }  

  ionViewDidLoad() {
    console.log('ionViewDidLoad Part1Page');
  }

}
