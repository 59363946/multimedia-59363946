import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Part1Page } from './part1';

@NgModule({
  declarations: [
    Part1Page,
  ],
  imports: [
    IonicPageModule.forChild(Part1Page),
  ],
})
export class Part1PageModule {}
