import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PricePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-price',
  templateUrl: 'price.html',
})
export class PricePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  slider = [
    {
      title:'ต่างชาติ: ผู้ใหญ่',
      image:"assets/imgs/13.svg"
    },
    {
      title:'ต่างชาติ: เด็ก',
      image:"assets/imgs/11.svg"
    },
    {
      title:'คนไทย: ผู้ใหญ่',
      image:"assets/imgs/10.svg"
    },
    {
      title:'คนไทย: เด็ก',
      image:"assets/imgs/12.svg"
    },
  ];

}
